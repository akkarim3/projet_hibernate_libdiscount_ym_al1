package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.control.GeneralControl;
import fr.afpa.utils.HibernateUtils;

public class DaoAnnonce {
	
	
    static Scanner in = new Scanner(System.in);
    static Session session = HibernateUtils.getSession();
    Transaction tx = session.beginTransaction();
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public static ArrayList<Annonce> recupSelfAnnonces(Utilisateur user) {
		
		ResultSet listAnnonce = null;
		ArrayList<Annonce> retourAnnonce = new ArrayList<Annonce>();
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM annonce where iduser = " + String.valueOf(user.getId());
			
			Statement stServices = conn.createStatement();
			
			listAnnonce = stServices.executeQuery(strQuery);
			
			Annonce annonce = null;
			
			while (listAnnonce.next()) {
					annonce = new Annonce();
					annonce.setId(listAnnonce.getInt(1));
					annonce.setTitre(listAnnonce.getString("titre"));
					annonce.setQuantite(listAnnonce.getInt("quantite"));
					annonce.setPrix(listAnnonce.getFloat("prix"));
					annonce.setPrixTotal(listAnnonce.getFloat("prixtotal"));
					annonce.setMaisonEdition(listAnnonce.getString("edition"));
					annonce.setIsbn(listAnnonce.getString("isbn"));
					annonce.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
					annonce.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));

					retourAnnonce.add(annonce);
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retourAnnonce;
	}
	
	public static ArrayList<Annonce> recupSelfAnnonces2(Utilisateur user) {
		Query search = session.getNamedQuery("findById2");
		search.setParameter("parametre", user.getId());
		
		
		
		
		return null;
	}
	
	public static void updateModif2(Annonce annonce) {
        String choix;
        String choix2;
        System.out.print("Modifier prix ? : Y | N ");
         choix = in.nextLine();
         System.out.print("Modifier quantit� ? : Y | N ");
         choix2 = in.nextLine();

        if (annonce != null && choix == "Y") {
                System.out.print("Nouveau prix? : ");
                int prixx = in.nextInt();

                System.out.print("-------------------Update  Prix-----------------");
                String hqlUpdate = "UPDATE annonce e Set e.prix = :param2 where e.id = :param";

                Query query=session.createQuery(hqlUpdate);
                query.setParameter("param2", prixx);
                query.setParameter("param", annonce.getId());
                }


            if (annonce != null && choix2 == "Y") {
                    System.out.print("Nouvelle quantit�? : ");
                    int quantite= in.nextInt();
                    System.out.print("-------------------Update  Quantit�-----------------");
                    String hqlUpdate2 = "UPDATE annonce e Set e.quantite = :param2 where e.id = :param";

                    Query query2=session.createQuery(hqlUpdate2);
                    query2.setParameter("param2", quantite);
                    query2.setParameter("param", annonce.getId());
                }

            else {
                System.out.print("Nous n'avons pas compris, r�essayer plus tard : ");
            }
	}
	
	public static void deleteAnnonce2(Annonce annonce) {
        Query q = session.createQuery("delete annonce where id = param");
        q.setParameter("param", annonce.getId());
        q.executeUpdate();
    }

    /**
     * 
     * @param titre
     * @return
     */
	
    public static ArrayList<Annonce> recupAnnonceByTitre2(String titre) {
        Query p = session.getNamedQuery("findByTitre");
        p.setParameter("param",titre);
        ArrayList<Annonce> listeAnn = (ArrayList<Annonce>) p.getResultList();
        for (Annonce annonce : listeAnn) {
            System.out.println(annonce);
        }
        return listeAnn;

    }
	/**
	 * 
	 * @param annonce
	 * @return
	 */
	public static Annonce recupModifAnnonce(Annonce annonce) {
		
		ResultSet listAnnonce = null;
		Annonce annonceRecup = null;
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM annonce where idannonce = " + String.valueOf(annonce.getId());
			
			Statement stServices = conn.createStatement();
			
			listAnnonce = stServices.executeQuery(strQuery);
			
			
			while (listAnnonce.next()) {
				annonceRecup = new Annonce();
				annonceRecup.setId(listAnnonce.getInt(1));
				annonceRecup.setTitre(listAnnonce.getString("titre"));
				annonceRecup.setQuantite(listAnnonce.getInt("quantite"));
				annonceRecup.setPrix(listAnnonce.getFloat("prix"));
				annonceRecup.setPrixTotal(listAnnonce.getFloat("prixtotal"));
				annonceRecup.setMaisonEdition(listAnnonce.getString("edition"));
				annonceRecup.setIsbn(listAnnonce.getString("isbn"));
				annonceRecup.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
				annonceRecup.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));

				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annonceRecup;
	}
	
	
	/**
	 * 
	 * @param annonce
	 */
	public static void insertInto(Annonce annonce) {
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "INSERT INTO annonce(idannonce ,titre ,quantite ,prix ,prixtotal ,remise ,edition, isbn, dateedition, dateannonce, iduser, ville)"
					+ " values (nextval	('incrementannnonce'), '" 
					+ annonce.getTitre() + "', "
					+ annonce.getQuantite() + ", "
					+ annonce.getPrix() + ", "
					+ annonce.getPrixTotal() + ", "
					+ annonce.getRemise() + ", '"
					+ annonce.getMaisonEdition() + "', '"
					+ annonce.getIsbn() + "', '"
					+ annonce.getDateEdition() + "', '"
					+ annonce.getDateAnnonce() + "', "
					+ GeneralControl.getUtilisateur().getId()+ ", '"
					+ GeneralControl.getUtilisateur().getAdresse().getVille() + "') ";
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param annonce
	 */
	public static void updateModif(Annonce annonce) {
		
		if (annonce != null) {
			try {
					
				String driverName = "org.postgresql.Driver";
				Class.forName(driverName);
				
				Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
				conn.setAutoCommit(false);
				
				String strQuery = "UPDATE annonce SET "
						+ "titre = '" + annonce.getTitre() + "',"
						+ "quantite  =" + String.valueOf(annonce.getQuantite()) + ","
						+ "prix =" + String.valueOf(annonce.getPrix()) + ","
						+ "prixtotal =" + String.valueOf(annonce.getPrixTotal()) + ","
						+ "remise =" + String.valueOf(annonce.getRemise()) + ","
						+ "edition = '" + annonce.getMaisonEdition() + "',"
						+ "isbn = '" + annonce.getIsbn() + "',"
						+ "dateedition = '" + annonce.getDateEdition() + "',"
						+ "dateannonce = '" + annonce.getDateAnnonce() + "'"
						+ " where idannonce = " + String.valueOf(annonce.getId());
				
				Statement stServices = conn.createStatement();
				stServices.executeUpdate(strQuery);
				
				stServices.close();
				conn.commit();
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Annonce modifi�e ! ");
		}else {
			System.out.println("Aucune modification apport�e.");
		}
	}

	/**
	 * 
	 * @param annonce
	 */
	public static void deleteAnnonce(Annonce annonce) {
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "DELETE FROM annonce where idannonce = " + annonce.getId();
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
			
			System.out.println("Annonce Supprim�e !");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * 
	 * @param titre
	 * @return
	 */
	public static ArrayList<Annonce> recupAnnonceByTitre(String titre) {
		
		ResultSet listAnnonce = null;
		ArrayList<Annonce> retourAnnonce = new ArrayList<Annonce>();
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM annonce where titre like '%" + titre + "%'";
			
			Statement stServices = conn.createStatement();
			
			listAnnonce = stServices.executeQuery(strQuery);
			
			Annonce annonce = null;
			
			while (listAnnonce.next()) {
					annonce = new Annonce();
					annonce.setId(listAnnonce.getInt(1));
					annonce.setTitre(listAnnonce.getString("titre"));
					annonce.setQuantite(listAnnonce.getInt("quantite"));
					annonce.setPrix(listAnnonce.getFloat("prix"));
					annonce.setPrixTotal(listAnnonce.getFloat("prixtotal"));
					annonce.setMaisonEdition(listAnnonce.getString("edition"));
					annonce.setIsbn(listAnnonce.getString("isbn"));
					annonce.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
					annonce.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));

					retourAnnonce.add(annonce);
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retourAnnonce;
	}
	
	/**
	 * 
	 * @param isbn
	 * @return
	 */
	public static ArrayList<Annonce> listerAnnonceISBN(String isbn) {
		
		ResultSet listAnnonce = null;
		ArrayList<Annonce> retourAnnonce = new ArrayList<Annonce>();
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM annonce where isbn like '%" + isbn+"%'";
			
			Statement stServices = conn.createStatement();
			
			listAnnonce = stServices.executeQuery(strQuery);
			
			Annonce annonce = null;
			
			while (listAnnonce.next()) {
					annonce = new Annonce();
					annonce.setId(listAnnonce.getInt(1));
					annonce.setTitre(listAnnonce.getString("titre"));
					annonce.setQuantite(listAnnonce.getInt("quantite"));
					annonce.setPrix(listAnnonce.getFloat("prix"));
					annonce.setPrixTotal(listAnnonce.getFloat("prixtotal"));
					annonce.setMaisonEdition(listAnnonce.getString("edition"));
					annonce.setIsbn(listAnnonce.getString("isbn"));
					annonce.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
					annonce.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));
					

					retourAnnonce.add(annonce);
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retourAnnonce;
	}

	/**
	 * 
	 * @param ville
	 * @return
	 */
	public static ArrayList<Annonce> listerAnnonceVille(String ville) {
	
	ResultSet listAnnonce = null;
	ArrayList<Annonce> retourAnnonce = new ArrayList<Annonce>();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
		
		String strQuery = "SELECT * FROM annonce where ville like '%" + ville+"%'";
		
		Statement stServices = conn.createStatement();
		
		listAnnonce = stServices.executeQuery(strQuery);
		
		Annonce annonce = null;
		
		while (listAnnonce.next()) {
				annonce = new Annonce();
				annonce.setId(listAnnonce.getInt(1));
				annonce.setTitre(listAnnonce.getString("titre"));
				annonce.setQuantite(listAnnonce.getInt("quantite"));
				annonce.setPrix(listAnnonce.getFloat("prix"));
				annonce.setPrixTotal(listAnnonce.getFloat("prixtotal"));
				annonce.setMaisonEdition(listAnnonce.getString("edition"));
				annonce.setIsbn(listAnnonce.getString("isbn"));
				annonce.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
				annonce.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));
				

				retourAnnonce.add(annonce);
			}
		conn.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return retourAnnonce;
}
	
}
