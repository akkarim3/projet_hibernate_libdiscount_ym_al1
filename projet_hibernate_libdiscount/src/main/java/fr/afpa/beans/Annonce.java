package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@NamedQuery(name= "findById", query= "from Utilisateur p where p.id = :parametre")
@NamedQuery(name= "findById2", query= "from annonce p where p.id = :parametre")

@Entity
public class Annonce {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String titre;
    @NonNull
    private int quantite;
    @NonNull
    private float prix;
    @NonNull
    private float prixTotal;
    @NonNull
    private float remise;
    @NonNull
    private String maisonEdition;
    @NonNull
    private String isbn;
    @NonNull
    private LocalDate dateEdition;
    @NonNull
    private LocalDate dateAnnonce;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Utilisateur utilisateur;
}
