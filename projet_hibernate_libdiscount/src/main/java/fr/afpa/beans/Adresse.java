package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@NamedQuery(name= "findByIdUser", query= "from adresse p where p.id = :idadresse")

@Entity
public class Adresse {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column
	private String rue;
	@Column
	private String ville;
	
	public Adresse(int id) {
		
		this.id = id;
	}
	public Adresse(String rue, String ville) {
		
		this.rue = rue;
		this.ville = ville;
	}
	
	
	
}
