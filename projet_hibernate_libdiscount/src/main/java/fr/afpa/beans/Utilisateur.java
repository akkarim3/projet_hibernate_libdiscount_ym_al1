package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@NamedQuery(name= "findByIdUser", query= "from utilisateur p where p.id = :iduser")
@NamedQuery(name= "findByNameUser", query= "from utilisateur p where p.nom = :nomuser")

@Entity
public class Utilisateur {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@NotNull
	@Column
	private String nom;
	@NotNull
	@Column
	private String prenom;
	@NotNull
	@Column
	private String mail;
	@NotNull
	@Column
	private String telephone;
	@NotNull
	@Column
	private String nomLibrairie;
	@NotNull
	@Column
	private Connexion login;
	
	@OneToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="fk_Adresse")
	@NotNull
	@Column
	private Adresse adresse;
	
	@OneToMany(mappedBy="utilisateur")
	private List<Annonce> annonce;
	
	

	public Utilisateur(String nom, String prenom, String mail, String telephone, String nomLibrairie, Connexion login,
			Adresse adresse, List<Annonce> annonce) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = telephone;
		this.nomLibrairie = nomLibrairie;
		this.login = login;
		this.adresse = adresse;
		this.annonce = annonce;
	}



	public Utilisateur(int id) {
		super();
		this.id = id;
	}



	public Utilisateur(String nom, String prenom, String mail, String telephone, String nomLibrairie, Connexion login,
			Adresse adresse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = telephone;
		this.nomLibrairie = nomLibrairie;
		this.login = login;
		this.adresse = adresse;
	}
	
	
	
}
