package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Connexion {

	@Column
	private String login;
	@Column
	private String mdp;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idUser;
	
}
