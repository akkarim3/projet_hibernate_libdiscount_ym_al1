package fr.afpa.control;

public class SaisieControl {
	
	public static boolean date(String date) {
		//AAAA-MM-DD
		//0123-56-89
		
		if (date.charAt(4) != '-' || date.charAt(7) != '-') {
			return false;
		}
		
		for (int i = 0; i < 4; i++) {
			if (!(Character.isDigit(date.charAt(i)))) {
				return false;
			}
		}
		
		
		for (int i = 5; i < 7; i++) {
			if (!(Character.isDigit(date.charAt(i)))) {
				return false;
			}
		}
		
		for (int i = 8; i < date.length(); i++) {
			if (!(Character.isDigit(date.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean remise(float remise) {
		if (remise < 0 || remise > 100) {
			return false;
		}
		return true;
	}
	
	public static boolean prix(float prix) {
		if (prix < 0) {
			return false;
		}
		return true;
	}

	public static boolean justStr(String str) {
		
		for (int i = 0; i < str.length(); i++) {
			if (!(Character.isAlphabetic(str.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean justNumeric(String numeric) {
		
		for (int i = 0; i < numeric.length(); i++) {
			if (!(Character.isDigit(numeric.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
	
	
}
