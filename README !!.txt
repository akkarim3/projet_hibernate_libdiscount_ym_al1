

Avant de lancer le .jar :



--> BDD :

- Ouvrez votre interface PGadmin sur votre navigateur

- Créez un superutilisateur avec, pour identifiant : "libdatahibernate", et mot de passe : "libdatahibernate"

- Créez une base de données se nommant : "libdatahibernate" et "libdatahibernate" pour OWNER

- Ouvrez votre logiciel client, DBeaver par exemple et connectez-vous à votre base de données



--> Une fois toutes ces étapes faites :

- Allez dans le dossier principal du projet, puis dans le dossier "target"

- Copiez-Collez "projet_hibernate_libdiscount" sur votre bureau

- Ouvrez un cmd (invite de commandes) et allez dans le répertoire de votre bureau -> "cd Desktop" ou "cd Bureau" selon votre machine

- Entrez la commande "java -jar projet_hibernate_libdiscount.jar"

- A partir de la vous pouvez créer votre compte et utiliser le reste de l'application